import csv
from gc import DEBUG_SAVEALL
from hashlib import sha3_512
from threading import local
import time
import sys
import numpy as np
import scipy as sp
from scipy.optimize import curve_fit
from scipy import signal
from scipy.fft import fftshift
from scipy.signal import butter, lfilter, freqz
import filterpy
from filterpy.kalman import KalmanFilter
from filterpy.kalman import UnscentedKalmanFilter
from filterpy.kalman import MerweScaledSigmaPoints
from filterpy.common import Q_discrete_white_noise
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
# plt.style.use('seaborn-whitegrid')
from scipy.interpolate import UnivariateSpline

def print_data(file):
    with open(file) as file:
        csv_reader = csv.reader(file, delimiter=',')
        row_count = 0
        for row in csv_reader:
            if row_count == 0:
                print(f'Reading ' + file.name)
                row_count += 1
            else:
                print(f'Time: \t{row[0]}, x:\t{row[1]}, y:\t{row[2]}, z:\t{row[3]}')
                row_count += 1
        print(f'Processed {row_count-1} lines.')

def read_data(file):
    with open(file) as file:
        csv_reader = csv.reader(file, delimiter=',')
        row_cnt = 0
        res = []
        for row in csv_reader:
            if row_cnt == 0:
                print(f'Reading ' + file.name)
                row_cnt += 1
            else:
                tuple = np.array([np.float(row[0]), np.float(row[1]), np.float(row[2]), np.float(row[3])])
                res.append(tuple)
                row_cnt += 1
        print(f'Processed {row_cnt-1} lines.')
        res = np.array(res)
    return res, row_cnt-1

def validate(est, act):
    cnt = min(len(est),len(act))
    t = []
    ydiff = []
    zdiff = []
    for i in range(cnt):
        t.append(est[i][0])
        ydiff.append(est[i][2]-act[i][2])
        zdiff.append(est[i][3]-act[i][3])
    yRMS = rms(ydiff)
    zRMS = rms(zdiff)
    print(f"Y-RMS is: {yRMS} Z-RMS is: {zRMS}")

    return yRMS, zRMS

    

### Convert from local to global coordinates
# rpm = 80, w = 2*pi*rpm/60 = pi*8/3
# theta = theta_0 + w*t (assume theta_0 = 0)
# ay = mY*cos(theta) + mZ*sin(theta)
# az = mZ*cos(theta) - mY*sin(theta)
# t0 = 2555.000023
def local_to_global(data, rpm, t0):
    w = 2*np.pi*rpm/60
    res = []

    for tuple in data:
        t = tuple[0]-t0
        theta = w*t
        ay = (tuple[2]*np.cos(theta) + tuple[3]*np.sin(theta))
        az = (tuple[3]*np.cos(theta) - tuple[2]*np.sin(theta))
        ax = tuple[2]
        tuple_new = [tuple[0], ax, ay, az]
        res.append(tuple_new)

    return res

def find_fit(data1, data2, cnt, deg):
    t  = [data1[i][0] for i in range(cnt)]
    y1 = [data1[i][2] for i in range(cnt)]
    y2 = [data2[i][2] for i in range(cnt)]
    z1 = [data1[i][3] for i in range(cnt)]
    z2 = [data2[i][3] for i in range(cnt)]

    y_fit = np.polyfit(y1,y2,deg)
    z_fit = np.polyfit(z1,z2,deg)

    # y1_RMS = np.sqrt(np.mean(np.power(y1,2)))
    # y2_RMS = np.sqrt(np.mean(np.power(y2,2)))

    # y_fit = y1_RMS/y2_RMS

    # z1_RMS = np.sqrt(np.mean(np.power(z1,2)))
    # z2_RMS = np.sqrt(np.mean(np.power(z2,2)))

    # z_fit = z1_RMS/z2_RMS

    # return y_fit, z_fit
    return np.poly1d(y_fit), np.poly1d(z_fit)

def D(a,t):
    return np.diff(a)/np.diff(t)

def rms(a):
    return np.sqrt(np.mean(np.power(a,2)))

def error(act,est):
    return 100*abs((act-est)/act)

def rmse(a,b):
    return np.sqrt(np.mean((a-b)**2))

def find_jerk(t, data):
    j = D(data,t)
    return np.mean(j), rms(j)

def butter_lowpass(cutoff, fs, order=5):
    return butter(order, cutoff, fs=fs, btype='low', analog=False)

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def filter(data, cutoff):
    t = data[:,0]
    xdata = data[:,1]
    ydata = data[:,2]
    zdata = data[:,3]
    
    order = 4
    fs = 2.0*10e4      # sample rate, Hz

    x = butter_lowpass_filter(xdata, cutoff, fs, order)
    y = butter_lowpass_filter(ydata, cutoff, fs, order)
    z = butter_lowpass_filter(zdata, cutoff, fs, order) 

    return x,y,z

def selectData(MEA, EST, T):

    file_loc = [0, "../data/DL1/SSS10023_026_T", "../data/DL2/SSS10024_012_T", "../data/DL3/SSS10039_031_T"]

    initial = {4: (80,  2555.000023, 2555.000024, 2555.000047),
               6: (104, 4879.000022, 4879.000035, 4879.000021),
               7: (129, 5190.000038, 5190.000034, 5190.000026),
               8: (151, 5466.000011, 5466.000026, 5466.000037),
               9: (172, 5789.000048, 5789.000012, 5789.000004)}
    
    return file_loc[MEA]+f'{T}.csv', file_loc[EST]+f'{T}.csv', initial[T][MEA], initial[T][EST], initial[T][0]




# Select datafile location and initial parameters


est_sel = int(input("Enter the data set to be estimated (enter 2 for DL2): "))
mea_sel = int(input("Enter the data set used as measurement: (enter 1 for DL1): "))
t_sel = int(input("Enter the RPM setting (enter 4 for T4): "))
data1, data2, t01, t02, rpm = selectData(MEA=mea_sel, EST=est_sel, T=t_sel)

saveLoc = f"../output/DL{est_sel}/T{t_sel}/"
print(f'Using measurement data DL{mea_sel}, to estimate data on DL{est_sel}, at T = {t_sel}, rpm = {rpm}')

# Read data from csv to np array
data1_local, data1_cnt = read_data(data1)
data2_local, data2_cnt = read_data(data2)
cnt = min(data1_cnt, data2_cnt)

data1_local = data1_local[:cnt]
data2_local = data2_local[:cnt]
t = data1_local[:,0]

# Convert data from local coord to global coord
data1_global = np.array(local_to_global(data1_local, rpm, t01))
data2_global = np.array(local_to_global(data2_local, rpm, t02))


# Since we focus on lateral movement, only the data on y and z axis will be considered in future calculations 


# Low-pass filter to remove spikes
data1_xfiltered, data1_yfiltered, data1_zfiltered = filter(data1_global, 1250.0)
data2_xfiltered, data2_yfiltered, data2_zfiltered = filter(data2_global, 1250.0)
# data1_xfiltered, data1_yfiltered, data1_zfiltered = filter(data1_local, 1250.0)
# data2_xfiltered, data2_yfiltered, data2_zfiltered = filter(data2_local, 1250.0)

# Find transimissibility between measurement data1(dl1) and data2 to estimate(dl2/dl3)


# Fitting with polynomials
y_fit = np.poly1d(np.polyfit(data2_yfiltered, data1_yfiltered, 3))
z_fit = np.poly1d(np.polyfit(data2_zfiltered, data1_zfiltered, 3))


# Fitting with rms ratio
y_ratio = rms(data1_yfiltered)/rms(data2_yfiltered)
z_ratio = rms(data1_zfiltered)/rms(data2_zfiltered)
# y_ratio = np.mean(data1_yfiltered)/np.mean(data2_yfiltered)
# z_ratio = np.mean(data1_zfiltered)/np.mean(data2_zfiltered)


# Plotting original data

# y1 = data1_local[:,2]
# y2 = data2_local[:,2]
# z1 = data1_local[:,3]
# z2 = data2_local[:,3]
# s1 = np.sqrt(np.add(data1_local[:,2]**2, data1_local[:,3]**2))
# s2 = np.sqrt(np.add(data2_local[:,2]**2, data2_local[:,3]**2))

y1 = data1_global[:,2]
y2 = data2_global[:,2]
z1 = data1_global[:,3]
z2 = data2_global[:,3]
s1 = np.sqrt(np.add(data1_global[:,2]**2, data1_global[:,3]**2))
s2 = np.sqrt(np.add(data2_global[:,2]**2, data2_global[:,3]**2))


plt.figure(0, figsize=(20,10))
plt.plot(t, y1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, y2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Original data on Y-axis")
plt.savefig(saveLoc+"original_y")


plt.figure(1, figsize=(20,10))
plt.plot(t, z1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, z2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Original data on Z-axis")
plt.savefig(saveLoc+"original_z")

plt.figure(2, figsize=(20,10))
plt.plot(t, s1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, s2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Original data on Y+Z(magnitude)")
plt.savefig(saveLoc+"original_y+z")

plt.show()

print('Original data:')
print(f"{'Data' : ^15}{'Y-MEAN' : ^15}{'Z-MEAN' : ^15}{'Y+Z MEAN' :^15}")
str1 = f'MEA(DL{mea_sel})'
str2 = f'EST(DL{est_sel})'
print(f"{str1:^15}{format(np.mean(y1),'.5f') : ^15}{format(np.mean(z1), '.5f'):^15}{format(np.mean(s1), '.5f'):^15}")
print(f"{str2:^15}{format(np.mean(y2),'.5f') : ^15}{format(np.mean(z2), '.5f'):^15}{format(np.mean(s2), '.5f'):^15}")



# Plotting filtered data

y1 = data1_yfiltered
y2 = data2_yfiltered
z1 = data1_zfiltered
z2 = data2_zfiltered
s1 = np.sqrt(np.add(data1_yfiltered**2, data1_zfiltered**2))
s2 = np.sqrt(np.add(data2_yfiltered**2, data2_zfiltered**2))

plt.figure(3, figsize=(20,10))
plt.plot(t, y1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, y2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Filtered data on Y-axis")
plt.savefig(saveLoc+"filtered_y")

plt.figure(4, figsize=(20,10))
plt.plot(t, z1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, z2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Filtered data on Z-axis")
plt.savefig(saveLoc+"filtered_z")

plt.figure(5, figsize=(20,10))
plt.plot(t, s1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, s2, 'g-', label=f'EST(DL{est_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Filtered data on Y+Z(magnitude)") 
plt.savefig(saveLoc+"filtered_y+z")

plt.show()

print('Filtered data:')
print(f"{'Data' : ^15}{'Y-MEAN' : ^15}{'Z-MEAN' : ^15}{'Y+Z MEAN' :^15}")
str1 = f'MEA(DL{mea_sel})'
str2 = f'EST(DL{est_sel})'
print(f"{str1:^15}{format(np.mean(y1),'.5f') : ^15}{format(np.mean(z1), '.5f'):^15}{format(np.mean(s1), '.5f'):^15}")
print(f"{str2:^15}{format(np.mean(y2),'.5f') : ^15}{format(np.mean(z2), '.5f'):^15}{format(np.mean(s2), '.5f'):^15}")


# print(f'MEA Y MAX: {np.max(data1_yfiltered)}, MEA Z MAX: {np.max(data1_zfiltered)}, MEA Y+Z MAX: {np.max(np.sqrt(np.add(data1_yfiltered**2, data1_zfiltered**2)))}')
# print(f'EST Y MAX: {np.max(data2_yfiltered)}, EST Z MAX: {np.max(data2_zfiltered)}, EST Y+Z MAX: {np.max(np.sqrt(np.add(data2_yfiltered**2, data2_zfiltered**2)))}')

# 
# Plotting fitted data 

y1 = data1_yfiltered
y2 = y_ratio*data2_yfiltered
z1 = data1_zfiltered
z2 = z_ratio*data2_zfiltered
s1 = np.sqrt(np.add(data1_yfiltered**2, data1_zfiltered**2))
s2 = np.sqrt(np.add((y_ratio*data2_yfiltered)**2, (z_ratio*data2_zfiltered)**2))

plt.figure(6, figsize=(20,10))
plt.plot(t, y1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, y2, 'r-', label=f'EST(DL{est_sel})->MEA(DL{mea_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Fitting EST->MEA on Y-axis")
plt.savefig(saveLoc+"fitting_y")

plt.figure(7, figsize=(20,10))
plt.plot(t, z1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, z2, 'r-', label=f'EST(DL{est_sel})->MEA(DL{mea_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Fitting EST->MEA on Z-axis")
plt.savefig(saveLoc+"fitting_z")

plt.figure(8, figsize=(20,10))
plt.plot(t, s1, 'b-', label=f'MEA(DL{mea_sel})')
plt.plot(t, s2, 'r-', label=f'EST(DL{est_sel})->MEA(DL{mea_sel})')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("Fitting EST->MEA on Y+Z(magnitude)")
plt.savefig(saveLoc+"fitting_y+z")

plt.show()

print('\nFitting from EST to MEA:')
print(f"{'Data' : ^15}{'Y-MEAN' : ^15}{'Z-MEAN' : ^15}{'Y+Z MEAN' :^15}")
str1 = f'MEA(DL{mea_sel})'
str2 = f'EST(DL{est_sel})'
print(f"{str1:^15}{format(np.mean(y1),'.5f') : ^15}{format(np.mean(z1), '.5f'):^15}{format(np.mean(s1), '.5f'):^15}")
print(f"{str2:^15}{format(np.mean(y2),'.5f') : ^15}{format(np.mean(z2), '.5f'):^15}{format(np.mean(s2), '.5f'):^15}")


# Preparing data for KF



# State transition equation in matrix form

# State variable x[ay, jy, az, jz]
# Assumption: Jerk is constant in both directions therefore ak+1 = ak + j*dt
# def fx(x, dt):
#     F = np.array([[1, dt, 0, 0],
#                   [0, 1, 0, 0],
#                   [0, 0, 1, dt],
#                   [0, 0, 0, 1]], dtype=float)
#     return np.dot(F, x)


# State variable x[ay, jy, sy, az, jz, sz]
# Assumption: Jerk is not constant, Snap(derivative of jerk) is constant
def fx(x, dt):
    F = np.array([[1, dt, 0.5*dt*dt, 0, 0, 0],
                  [0, 1, dt, 0, 0, 0],
                  [0, 0, 1, 0, 0, 0],
                  [0, 0, 0, 1, dt, 0.5*dt*dt],
                  [0, 0, 0, 0, 1, dt],
                  [0, 0, 0, 0, 0, 1] ], dtype=float)
    return np.dot(F, x)

# Measurement equation in matrix form 
# x = y*transmissbility ratio
def mea_equ(a, b):
    def hx(x):
        return np.array([a*x[0], b*x[3]])
    return hx 

hx = mea_equ(y_ratio, z_ratio)


# Time step
dt = 0.00005
# Create sigma points to use in the filter
points = MerweScaledSigmaPoints(6, alpha=0.001, beta=2., kappa=-3)
# Creating unscented kf
kf = UnscentedKalmanFilter(dim_x=6, dim_z=2, dt=dt, fx=fx, hx=hx, points=points)

# Initial state including acceleration, jerk and snap in y and z direction
y0 = data2_yfiltered[0]
y1 = data2_yfiltered[1]
y2 = data2_yfiltered[2]
z0 = data2_zfiltered[0]
z1 = data2_zfiltered[1]
z2 = data2_zfiltered[2]

jy0 = (y1-y0)/dt
jy1 = (y2-y1)/dt
jz0 = (z1-z0)/dt
jz1 = (z2-z1)/dt

sy0 = (jy1-jy0)/dt 
sz0 = (jz1-jz0)/dt


# kf.x = np.array([y0, jy0, sy0, z0, jz0, sz0])
kf.x = np.array([0,0,0,0,0,0])

# Initial uncertainty
kf.P *= 0.2 

# Covariance matrix for measurement noise
mstd = 0.1
kf.R = np.diag([mstd**2, mstd**2]) # 1 standard

# Covariance matrix for process noise
pstd = 0.5
kf.Q = Q_discrete_white_noise(dim=3, dt=dt, var=pstd**2, block_size=2)

# Measurement data
zs = np.array(list(zip(data1_yfiltered,data1_zfiltered))) 

# Store the estimated data for each iteration
data2_est = data1_local

# Starting time CPU time
start = time.clock()

# Running UKF
print(f"\nStarted running UKF at {format(start, '.2f')}")

cnt = 0
for z in zs:
    kf.predict()
    kf.update(z)
    data2_est[cnt][1] = data1_xfiltered[cnt]
    data2_est[cnt][2] = kf.x[0]
    data2_est[cnt][3] = kf.x[3]
    cnt += 1
    
    # print(kf.x, 'log-likelihood', kf.log_likelihood)

# Ending time CPU time
end = time.clock()


# Plotting UKF estimate results
ymea = data1_yfiltered
yact = data2_yfiltered
yest = data2_est[:,2]

zmea = data1_zfiltered
zact = data2_zfiltered
zest = data2_est[:,3]

smea = np.sqrt(np.add(ymea**2,zmea**2))
sact = np.sqrt(np.add(yact**2,zact**2))
sest = np.sqrt(np.add(yest**2,zest**2))


y1 = yact
y2 = yest
z1 = zact
z2 = zest
s1 = sact
s2 = sest


# Plotting final results

plt.figure(9, figsize=(20,10))
plt.plot(t, y1, 'b-', label=f'ACT(DL{est_sel})')
plt.plot(t, y2, 'r-', label=f'EST(DL{est_sel})')
plt.xlabel('Time/sec')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("ACT/EST data after UKF on Y-axis")
plt.savefig(saveLoc+"final_y")

plt.figure(10, figsize=(20,10))
plt.plot(t, z1, 'b-', label=f'ACT(DL{est_sel})')
plt.plot(t, z2, 'r-', label=f'EST(DL{est_sel})')
plt.xlabel('Time/sec')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("ACT/EST data after UKF on Z-axis")
plt.savefig(saveLoc+"final_z")

plt.figure(11, figsize=(20,10))
plt.plot(t, s1, 'b-', label=f'ACT(DL{est_sel})')
plt.plot(t, s2, 'r-', label=f'EST(DL{est_sel})')
plt.axhline(rms(s1), color='b', label=f'ACT-RMS')
plt.axhline(rms(s2), color='r', label=f'EST-RMS')
plt.xlabel('Time/sec')
plt.grid()
plt.legend()
plt.xlabel("Time/s")
plt.ylabel("Accl/G")
plt.title("ACT/EST data after UKF on Y+Z(magnitude)")
plt.savefig(saveLoc+"final_y+z")

plt.show()

print(f"Ended running UKF at {format(end, '.2f')}")

print(f"Running UKF on DL{mea_sel} and DL{est_sel} at T{t_sel} takes CPU time: {format(end-start, '.2f')}")

print(f"mstd: {mstd}, pstd: {pstd}")

print(f"Final results after UKF: Using DL{mea_sel} to estimate DL{est_sel}, against actual DL{est_sel}, T = {t_sel}, rpm = {rpm}")
print(f"{'Data' : ^15}{'Y-MEAN' : ^15}{'Y-RMS:' :^15}{'Z-MEAN' : ^15}{'Z-RMS:' :^15}{'Y+Z MEAN' :^15}{'Y+Z-RMS:' :^15}")
str1 = f'ACT(DL{est_sel})'
str2 = f'EST(DL{est_sel})'
print(f"{f'ACT(DL{est_sel})':^15}{format(np.mean(y1),'.5f') : ^15}{format(rms(y1),'.5f') : ^15}{format(np.mean(z1), '.5f'):^15}{format(rms(z1),'.5f') : ^15}{format(np.mean(s1), '.5f'):^15}{format(rms(s1),'.5f') : ^15}")
print(f"{f'EST(DL{est_sel})':^15}{format(np.mean(y2),'.5f') : ^15}{format(rms(y2),'.5f') : ^15}{format(np.mean(z2), '.5f'):^15}{format(rms(z2),'.5f') : ^15}{format(np.mean(s2), '.5f'):^15}{format(rms(s2),'.5f') : ^15}")
print(f"{f'Error%':^15}{format(error(np.mean(y1),np.mean(y2)),'.2f') : ^15}{format(error(rms(y1),rms(y2)),'.2f') : ^15}{format(error(np.mean(z1),np.mean(z2)), '.2f'):^15}{format(error(rms(z1),rms(z2)),'.2f') : ^15}{format(error(np.mean(s1),np.mean(s2)), '.2f'):^15}{format(error(rms(s1),rms(s2)),'.2f') : ^15}")
print(f"RMSE: Y: {format(rmse(y1,y2), '.5f')}, Z:  {format(rmse(z1,z2),'.5f')}, Y+Z:  {format(rmse(s1,s2), '.5f')}")

print('\n')
print(f"{'Data' : ^15}{'Y-MAX' : ^15}{'Z-MAX' : ^15}{'Y+Z MAX' :^15}")
print(f"{str1:^15}{format(np.max(y1),'.5f') : ^15}{format(np.max(z1), '.5f'):^15}{format(np.max(s1), '.5f'):^15}")
print(f"{str2:^15}{format(np.max(y2),'.5f') : ^15}{format(np.max(z2), '.5f'):^15}{format(np.max(s2), '.5f'):^15}")

orig_stdout = sys.stdout

with open(saveLoc+"benchmark.txt", 'w') as f:
    sys.stdout = f
    print(f"Running UKF on DL{mea_sel} and DL{est_sel} at T{t_sel} takes CPU time: {format(end-start, '.2f')}")

    print(f"mstd: {mstd}, pstd: {pstd}")

    print(f"Final results after UKF: Using DL{mea_sel} to estimate DL{est_sel}, against actual DL{est_sel}, T = {t_sel}, rpm = {rpm}")
    print(f"{'Data' : ^15}{'Y-MEAN' : ^15}{'Y-RMS:' :^15}{'Z-MEAN' : ^15}{'Z-RMS:' :^15}{'Y+Z MEAN' :^15}{'Y+Z-RMS:' :^15}")
    print(f"{f'ACT(DL{est_sel})':^15}{format(np.mean(y1),'.5f') : ^15}{format(rms(y1),'.5f') : ^15}{format(np.mean(z1), '.5f'):^15}{format(rms(z1),'.5f') : ^15}{format(np.mean(s1), '.5f'):^15}{format(rms(s1),'.5f') : ^15}")
    print(f"{f'EST(DL{est_sel})':^15}{format(np.mean(y2),'.5f') : ^15}{format(rms(y2),'.5f') : ^15}{format(np.mean(z2), '.5f'):^15}{format(rms(z2),'.5f') : ^15}{format(np.mean(s2), '.5f'):^15}{format(rms(s2),'.5f') : ^15}")
    print(f"{f'Error%':^15}{format(error(np.mean(y1),np.mean(y2)),'.2f') : ^15}{format(error(rms(y1),rms(y2)),'.2f') : ^15}{format(error(np.mean(z1),np.mean(z2)), '.2f'):^15}{format(error(rms(z1),rms(z2)),'.2f') : ^15}{format(error(np.mean(s1),np.mean(s2)), '.2f'):^15}{format(error(rms(s1),rms(s2)),'.2f') : ^15}")
    print(f"RMSE: Y: {format(rmse(y1,y2), '.5f')}, Z:  {format(rmse(z1,z2),'.5f')}, Y+Z:  {format(rmse(s1,s2), '.5f')}")

    print('\n')
    print(f"{'Data' : ^15}{'Y-MAX' : ^15}{'Z-MAX' : ^15}{'Y+Z MAX' :^15}")
    print(f"{str1:^15}{format(np.max(y1),'.5f') : ^15}{format(np.max(z1), '.5f'):^15}{format(np.max(s1), '.5f'):^15}")
    print(f"{str2:^15}{format(np.max(y2),'.5f') : ^15}{format(np.max(z2), '.5f'):^15}{format(np.max(s2), '.5f'):^15}")
    
    sys.stdout = orig_stdout