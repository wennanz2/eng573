# ENG573 Capstone Project

# Drilling tool motion estimation model with Unscented Kalman Filter
    
## Team member:  
Shengyu Ge  (shengyu3@illinois.edu)\
Wennan Zhai (wennanz2@illinois.edu)

## Language: 
Python3
## Requirements:
>pip3 install numpy

>pip3 install scipy

>pip3 install filterpy

## How to run:

In /kf/, run
>python3 main.py

and type the input arguments following the prompt:
>Enter the data set to be estimated (enter 2 for DL2):

>Enter the data set used as measurement: (enter 1 for DL1):

>Enter the RPM setting (enter 4 for T4):

## Results:
Output figures and benchmarks are stored in /output/DL*/T*/

original_* is the data after coordinate conversion

filter_* is the data after low-pass filtering

final_* is the final results after UKF

benchmark.txt saves the calculated mean and rms etc for final results